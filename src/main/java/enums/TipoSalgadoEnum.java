package enums;

public enum TipoSalgadoEnum {

	ASSADO(13), FRITO(23), CONGELADO(34);

	private Integer tipo;

	private TipoSalgadoEnum(Integer tipo) {
		this.tipo = tipo;
	}

	public Integer getTipo() {
		return tipo;
	}

	public void setTipo(Integer tipo) {
		this.tipo = tipo;
	}

	
	public static TipoSalgadoEnum valueOfCodigo(Integer tipo) {
		for (TipoSalgadoEnum tipoSalgadoEnum : values()) {
			if (tipoSalgadoEnum.getTipo().equals(tipo)) {
				return tipoSalgadoEnum;
			}
		}
		throw new IllegalArgumentException("Codigo no vlido: " + tipo);
	}

	

}
