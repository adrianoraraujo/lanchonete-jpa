package consultas;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;

import org.jboss.logging.Logger;

import domain.Cliente;
import domain.Endereco;
import domain.Produto;
import domain.Salgado;
import enums.TipoSalgadoEnum;

public class Popular
{

	public static void main(String[] args)
	{
 		EntityManagerFactory emf = null;

		emf = Persistence.createEntityManagerFactory("lanchonete");
		EntityManager em = emf.createEntityManager();
		em.getTransaction().begin();
		CriarProdutoEcliente(em);
		em.getTransaction().commit();
		ExcluirCliente(em);
		UpdateCliente(em);
		ConsultaClienteDto(em);

		// 7

		// 8

		em.close();

	}

	private static void ExcluirCliente(EntityManager em)
	{
		em.getTransaction().begin();

		em.remove(em.getReference(Cliente.class, "12345"));
		em.getTransaction().commit();

	}

	private static void UpdateCliente(EntityManager em)
	{

		Cliente cliente = em.find(Cliente.class, "54321");
		em.getTransaction().begin();
		cliente.setNome("Jorge");
		em.getTransaction().commit();

	}

	private static void ConsultaClienteDto(EntityManager em) {
		String jpql = "select new " + ClienteConsultaDto.class.getCanonicalName()
				+ "(c.cpf, c.nome) FROM Cliente c left JOIN c.enderecos e where e.codigo=null";
		TypedQuery<ClienteConsultaDto> query = em.createQuery(jpql, ClienteConsultaDto.class);
		List<ClienteConsultaDto> clientesDto = query.getResultList();
		System.out.println(clientesDto);
	
		
	}
	
	private static void ConsultaCoxinha(EntityManager em) {
		
	//	String jpql = "select "
				
				//Utilizando JPA, crie uma consulta que retorne os clientes que fizeram pedido do produto com nome �Coxinha�. Seu m�todo deve ter retorno List<Cliente> e n�o deve exibir os dados.
	}
	private static void CriarProdutoEcliente(EntityManager em)
	{

		Produto p1 = new Produto("Bolo", 10.00, 1);
		Salgado s1 = new Salgado("Coxinha", 5.00, 1, TipoSalgadoEnum.FRITO);
		System.out.println("step1");
		em.persist(s1);
		em.persist(p1);
		System.out.println("step2");

		ArrayList<Produto> produtos1 = new ArrayList<>();
		produtos1.add(s1);
		ArrayList<Produto> produtos2 = new ArrayList<>();
		produtos2.add(p1);
		System.out.println("step3");
		ArrayList<Endereco> endere�os1 = new ArrayList<>();
		ArrayList<Endereco> endere�os2 = new ArrayList<>();
		System.out.println("step4");
		Endereco e1 = new Endereco("rua das pedras", "6", "Minha rola");
		Endereco e2 = new Endereco("rua das cajazeiras", "10", "Minha Taka");
		em.persist(e2);
		em.persist(e1);
		System.out.println("step5");
		endere�os1.add(e1);
		endere�os2.add(e2);
		System.out.println("step6");
		Cliente c1 = new Cliente("12345", "Sergio", endere�os1, produtos1);
		Cliente c2 = new Cliente("54321", "Adriano", endere�os2, produtos2);
		Cliente c3 = new Cliente("3326", "Cicciolina", null, produtos1);
		System.out.println("step7");
		em.persist(c1);
		em.persist(c2);
		em.persist(c3);
		System.out.println("step8");

	}

	private static final Logger logger = Logger.getLogger(Popular.class);

}
