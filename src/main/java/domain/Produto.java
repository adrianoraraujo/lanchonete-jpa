package domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "tab_produto")
@SequenceGenerator(name = "seq_codigo", sequenceName = "seq_codigo", allocationSize = 1)
@Inheritance(strategy = InheritanceType.JOINED)

public class Produto
{

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq_codigo")
	@Column(name = "codigo", nullable = false)
	private Integer codigo; // integer (sequence) - not null

	@Column(name = "nome", columnDefinition = "varchar(30)", nullable = false)
	private String nome; // varchar(30) - not null

	@Column(name = "valor_unitario", precision = 10, scale = 2, nullable = false)
	private Double valorUnitario; // numeric(10,2) - not null

	@Column(name = "qtd_minima", nullable = false)
	private Integer qtdMinima; // integer - not null

	public Produto()
	{
	}

	public Produto(String nome, Double valorUnitario, Integer qtdMinima)
	{
		super();
		this.nome = nome;
		this.valorUnitario = valorUnitario;
		this.qtdMinima = qtdMinima;
	}

	public Integer getCodigo()
	{
		return codigo;
	}

	public void setCodigo(Integer codigo)
	{
		this.codigo = codigo;
	}

	public String getNome()
	{
		return nome;
	}

	public void setNome(String nome)
	{
		this.nome = nome;
	}

	public Double getValorUnitario()
	{
		return valorUnitario;
	}

	public void setValorUnitario(Double valorUnitario)
	{
		this.valorUnitario = valorUnitario;
	}

	public Integer getQtdMinima()
	{
		return qtdMinima;
	}

	public void setQtdMinima(Integer qtdMinima)
	{
		this.qtdMinima = qtdMinima;
	}

	@Override
	public String toString()
	{
		return "Produto [codigo=" + codigo + ", nome=" + nome + ", valorUnitario=" + valorUnitario + ", qtdMinima="
				+ qtdMinima + "]";
	}

}
