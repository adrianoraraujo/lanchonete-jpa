package domain;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="tab_cliente")
public class Cliente {

	@Id
	@Column(name = "cpf", columnDefinition = "char(11)", nullable = false)
	private String cpf; // char(11) - not null

	@Column(name = "nome", columnDefinition = "char(40)", nullable = false)
	private String nome; // char(40) - not null

	@OneToMany
	private List<Endereco> enderecos;

	@ManyToMany()
	private List<Produto> produtos;

	public Cliente() {
		// TODO Auto-generated constructor stub
	}

	public Cliente(String cpf, String nome, List<Endereco> enderecos, List<Produto> produtos) {
		super();
		this.cpf = cpf;
		this.nome = nome;
		this.enderecos = enderecos;
		this.produtos = produtos;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Endereco> getEnderecos() {
		return enderecos;
	}

	public void setEnderecos(List<Endereco> enderecos) {
		this.enderecos = enderecos;
	}

	public List<Produto> getProdutos() {
		return produtos;
	}

	public void setProdutos(List<Produto> produtos) {
		this.produtos = produtos;
	}

	@Override
	public String toString() {
		return "Cliente [cpf=" + cpf + ", nome=" + nome + ", enderecos=" + enderecos + ", produtos=" + produtos + "]";
	}

}
