package domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@SequenceGenerator(name = "seq_cod", sequenceName = "seq_cod", allocationSize= 1)
@Table(name = "tab_endereco")
public class Endereco {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE , generator = "seq_cod")
	@Column(name = "codigo", columnDefinition = "INTEGER", nullable = false)
	private Integer codigo; // integer (sequence) - not null

	@Column(name = "rua", columnDefinition = "varchar(40)", nullable = false)
	private String rua; // char(40) - not null

	@Column(name = "numero", columnDefinition = "varchar(40)", nullable = false)
	private String numero; // varchar(40) - not null

	@Column(name = "bairro", columnDefinition = "varchar(40)", nullable = false)
	private String bairro; // varchar(40) - not null

	public Endereco() {
		// TODO Auto-generated constructor stub
	}

	public Endereco(String rua, String numero, String bairro) {
		super();
		this.rua = rua;
		this.numero = numero;
		this.bairro = bairro;
	}

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getRua() {
		return rua;
	}

	public void setRua(String rua) {
		this.rua = rua;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	@Override
	public String toString() {
		return "Endereco [codigo=" + codigo + ", rua=" + rua + ", numero=" + numero + ", bairro=" + bairro + "]";
	}

}
