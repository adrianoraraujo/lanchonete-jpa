package domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import enums.TipoSalgadoEnum;

@Entity
@Table(name = "tab_salgado")
public class Salgado extends Produto {
	@Column(name = "tipo")
	private TipoSalgadoEnum tipo;

	public Salgado() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Salgado(String nome, Double valorUnitario, Integer qtdMinima, TipoSalgadoEnum tipo) {
		super(nome, valorUnitario, qtdMinima);
		this.tipo = tipo;
	}

	public Salgado(TipoSalgadoEnum tipo) {
		super();
		this.tipo = tipo;
	}

	public TipoSalgadoEnum getTipo() {
		return tipo;
	}

	public void setTipo(TipoSalgadoEnum tipo) {
		this.tipo = tipo;
	}

	@Override
	public String toString() {
		return "Salgado [tipo=" + tipo + "]";
	}

}
